# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Andi Chandler <andi@gowling.com>, 2014
# Richard Shaylor <rshaylor@me.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-12-16 15:03+0100\n"
"PO-Revision-Date: 2015-12-17 09:29+0000\n"
"Last-Translator: carolyn <carolyn@anhalt.org>\n"
"Language-Team: English (United Kingdom) (http://www.transifex.com/otf/torproject/language/en_GB/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en_GB\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: mat-gui:64 mat-gui:415 mat-gui:438
msgid "Ready"
msgstr "Ready"

#: mat-gui:133
msgid "Choose files"
msgstr "Choose files"

#: mat-gui:141
msgid "All files"
msgstr "All files"

#: mat-gui:147
msgid "Supported files"
msgstr "Supported files"

#: mat-gui:164 mat-gui:359 mat-gui:410 mat-gui:434 mat-gui:436
#: data/mat.glade:480
msgid "Clean"
msgstr "Clean"

#: mat-gui:165
msgid "No metadata found"
msgstr "No metadata found"

#: mat-gui:167 mat-gui:412
msgid "Dirty"
msgstr "Dirty"

#: mat-gui:172
#, python-format
msgid "%s's metadata"
msgstr "%s's metadata"

#: mat-gui:183
msgid "Trash your meta, keep your data"
msgstr "Delete your meta, keep your data"

#: mat-gui:188
msgid "Website"
msgstr "Website"

#: mat-gui:214
msgid "Preferences"
msgstr "Preferences"

#: mat-gui:227
msgid "Reduce PDF quality"
msgstr "Reduce PDF quality"

#: mat-gui:230
msgid "Reduce the produced PDF size and quality"
msgstr "Reduce the produced PDF size and quality"

#: mat-gui:233
msgid "Add unsupported file to archives"
msgstr "Add unsupported file to archives"

#: mat-gui:236
msgid "Add non-supported (and so non-anonymised) file to output archive"
msgstr "Add non-supported (and so non-anonymised) file to output archive"

#: mat-gui:275
msgid "Unknown"
msgstr "Unknown"

#: mat-gui:318
msgid "Not-supported"
msgstr "Not-supported"

#: mat-gui:332
msgid "Harmless fileformat"
msgstr "Harmless fileformat"

#: mat-gui:334
msgid "Cant read file"
msgstr ""

#: mat-gui:336
msgid "Fileformat not supported"
msgstr "Fileformat not supported"

#: mat-gui:339
msgid "These files can not be processed:"
msgstr "These files can not be processed:"

#: mat-gui:344 mat-gui:373 data/mat.glade:519
msgid "Filename"
msgstr "Filename"

#: mat-gui:346
msgid "Reason"
msgstr "Reason"

#: mat-gui:358
msgid "Non-supported files in archive"
msgstr "Non-supported files in archive"

#: mat-gui:372
msgid "Include"
msgstr "Include"

#: mat-gui:390
#, python-format
msgid "MAT is not able to clean the following files, found in the %s archive"
msgstr "MAT is not able to clean the following files, found in the %s archive"

#: mat-gui:406
#, python-format
msgid "Checking %s"
msgstr "Checking %s"

#: mat-gui:421
#, python-format
msgid "Cleaning %s"
msgstr "Cleaning %s"

#: data/mat.glade:26 data/mat.glade:196
msgid "Metadata"
msgstr "Metadata"

#: data/mat.glade:85
msgid "Name"
msgstr "Name"

#: data/mat.glade:99
msgid "Content"
msgstr "Content"

#: data/mat.glade:129
msgid "Supported formats"
msgstr "Supported formats"

#: data/mat.glade:185
msgid "Support"
msgstr "Support"

#: data/mat.glade:207
msgid "Method"
msgstr "Method"

#: data/mat.glade:218
msgid "Remaining"
msgstr "Remaining"

#: data/mat.glade:247
msgid "Fileformat"
msgstr "Fileformat"

#: data/mat.glade:326
msgid "_File"
msgstr "_File"

#: data/mat.glade:375
msgid "_Edit"
msgstr "_Edit"

#: data/mat.glade:421
msgid "_Help"
msgstr "_Help"

#: data/mat.glade:467
msgid "Add"
msgstr "Add"

#: data/mat.glade:536
msgid "State"
msgstr "State"
